import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/EditProfile',
    name: 'EditProfile',
    component: () => import('../views/Input1')
  },
  {
    path: '/App',
    name: 'App',
    component: () => import('../views/Page1.vue')
  },

  {
    path: '/Manager',
    name: 'Manager',
    component: () => import('../components/Manager.vue')
  },
  {
    path: '/DataTable',
    name: 'DataTable',
    component: () => import('../views/DataTable.vue')
  },
  {
    path: '/Manager',
    name: 'Manager',
    component: () => import('../components/Manager.vue')
  },
  {
    path: '/ProductTable',
    name: 'Product Table',
    component: () => import('../views/product/ProductTable.vue')
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/product',
    name: 'Product',
    component: () => import('../views/Product.vue')
  },
  {
    path: '/successpage',
    name: 'successpage',
    component: () => import('../Page/SuccessPage.vue')
  },
  {
    path: '/userpage',
    name: 'userpage',
    component: () => import('../views/UserPage.vue')
  },
  // Toy
  {
    path: '/Toy',
    name: 'Toy',
    component: () => import('../Page/Toy.vue')
  },
  {
    path: '/AuctionToy/:_id',
    name: 'AuctionToy',
    component: () => import('../Page/Pagemin/AuctionToy')
  },
  {
    path: '/AuctionToy2',
    name: 'AuctionToy2',
    component: () => import('../Page/Pagemin/AuctionToy2')
  },
  {
    path: '/AuctionToy3',
    name: 'AuctionToy3',
    component: () => import('../Page/Pagemin/AuctionToy3')
  },
  {
    path: '/AuctionToy4',
    name: 'AuctionToy4',
    component: () => import('../Page/Pagemin/AuctionToy4')
  },

  // Sports
  {
    path: '/Sports',
    name: 'Sports',
    component: () => import('../Page/Sports')
  },
  {
    path: '/Sports1',
    name: 'Sports1',
    component: () => import('../Page/Pagemin/Sports1')
  },
  {
    path: '/Sports2',
    name: 'Sports2',
    component: () => import('../Page/Pagemin/Sports2')
  },
  {
    path: '/Sports3',
    name: 'Sports3',
    component: () => import('../Page/Pagemin/Sports3')
  },
  {
    path: '/Sports4',
    name: 'Sports4',
    component: () => import('../Page/Pagemin/Sports4')
  },
  // Cars_and_motorcycles
  {
    path: '/Cars_and_motorcycles',
    name: 'Cars_and_motorcycles',
    component: () => import('../Page/Cars_and_motorcycles')
  },
  {
    path: '/Cars1',
    name: 'Cars1',
    component: () => import('../Page/Pagemin/Cars1')
  },
  {
    path: '/Cars2',
    name: 'Cars2',
    component: () => import('../Page/Pagemin/Cars2')
  },
  {
    path: '/Cars3',
    name: 'Cars3',
    component: () => import('../Page/Pagemin/Cars3')
  },
  {
    path: '/Cars4',
    name: 'Cars4',
    component: () => import('../Page/Pagemin/Cars4')
  },
  // Fashion
  {
    path: '/Fashion',
    name: 'Fashion',
    component: () => import('../Page/Fashion')
  },
  {
    path: '/Fashion1',
    name: 'Fashion1',
    component: () => import('../Page/Pagemin/Fashion1')
  },
  {
    path: '/Fashion2',
    name: 'Fashion2',
    component: () => import('../Page/Pagemin/Fashion2')
  },
  {
    path: '/Fashion3',
    name: 'Fashion3',
    component: () => import('../Page/Pagemin/Fashion3')
  },
  {
    path: '/Fashion4',
    name: 'Fashion4',
    component: () => import('../Page/Pagemin/Fashion4')
  },
  // Jewelry_and_watches
  {
    path: '/Jewelry_and_watches',
    name: ' Jewelry_and_watches',
    component: () => import('../Page/Jewelry_and_watches')
  },
  {
    path: '/Watch_Jewelry1',
    name: 'Watch_Jewelry1',
    component: () => import('../Page/Pagemin/Watch_Jewelry1')
  },
  {
    path: '/Watch_Jewelry2',
    name: 'Watch_Jewelry2',
    component: () => import('../Page/Pagemin/Watch_Jewelry2')
  },
  {
    path: '/Watch_Jewelry3',
    name: 'Watch_Jewelry3',
    component: () => import('../Page/Pagemin/Watch_Jewelry3')
  },
  {
    path: '/Watch_Jewelry4',
    name: 'Watch_Jewelry4',
    component: () => import('../Page/Pagemin/Watch_Jewelry4')
  },

  // Writing_equipment_and_tools
  {
    path: '/Writing_equipment_and_tools',
    name: 'Writing_equipment_and_tools',
    component: () => import('../Page/Writing_equipment_and_tools')
  },
  {
    path: '/WritingInstruments1',
    name: 'WritingInstruments1',
    component: () => import('../Page/Pagemin/WritingInstruments1')
  },
  {
    path: '/WritingInstruments2',
    name: 'WritingInstruments2',
    component: () => import('../Page/Pagemin/WritingInstruments2')
  },
  {
    path: '/WritingInstruments3',
    name: 'WritingInstruments',
    component: () => import('../Page/Pagemin/WritingInstruments3')
  },
  {
    path: '/WritingInstruments4',
    name: 'WritingInstruments4',
    component: () => import('../Page/Pagemin/WritingInstruments4')
  },

  // Children_products
  {
    path: '/Children_products',
    name: 'Children_products',
    component: () => import('../Page/Children_products')
  },
  {
    path: '/Children1',
    name: 'Children1',
    component: () => import('../Page/Pagemin/Children1')
  },
  {
    path: '/Children2',
    name: 'Children2',
    component: () => import('../Page/Pagemin/Children2')
  },
  {
    path: '/Children3',
    name: 'Children3',
    component: () => import('../Page/Pagemin/Children3')
  },
  {
    path: '/Children4',
    name: 'Children4',
    component: () => import('../Page/Pagemin/Children4')
  },
  // Idol_merchandise
  {
    path: '/Idol_merchandise',
    name: 'Idol_merchandise',
    component: () => import('../Page/Idol_merchandise')
  },
  {
    path: '/Idol1',
    name: 'Idol1',
    component: () => import('../Page/Pagemin/Idol1')
  },
  {
    path: '/Idol2',
    name: 'Idol2',
    component: () => import('../Page/Pagemin/Idol2')
  },
  {
    path: '/Idol3',
    name: 'Idol3',
    component: () => import('../Page/Pagemin/Idol3')
  },
  {
    path: '/Idol4',
    name: 'Idol4',
    component: () => import('../Page/Pagemin/Idol4')
  },

  // Anime_and_manga_products
  {
    path: '/Anime_and_manga_products',
    name: 'Anime_and_manga_products',
    component: () => import('../Page/Anime_and_manga_products')
  },
  {
    path: '/Anime1',
    name: 'Anime1',
    component: () => import('../Page/Pagemin/Anime1')
  },
  {
    path: '/Anime2',
    name: 'Anime2',
    component: () => import('../Page/Pagemin/Anime2')
  },
  {
    path: '/Anime3',
    name: 'Anime3',
    component: () => import('../Page/Pagemin/Anime3')
  },
  {
    path: '/Anime4',
    name: 'Anime4',
    component: () => import('../Page/Pagemin/Anime4')
  },
  // Others
  {
    path: '/Others',
    name: ' Others',
    component: () => import('../Page/Others')
  },
  {
    path: '/Etc1',
    name: 'Etc1',
    component: () => import('../Page/Pagemin/Etc1')
  },
  {
    path: '/Etc2',
    name: 'Etc2',
    component: () => import('../Page/Pagemin/Etc2')
  },
  {
    path: '/Etc3',
    name: 'Etc3',
    component: () => import('../Page/Pagemin/Etc3')
  },
  {
    path: '/Etc4',
    name: 'Etc4',
    component: () => import('../Page/Pagemin/Etc4')
  }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
